package pl.codeleak.samples.thymeleaf;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeleafController {
    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("message", "Hello, Thymeleaf!");
        model.addAttribute("priority", "high");
        model.addAttribute("fruits", new String[]{"apple", "banana", "orange", "kiwi"});
        return "template";
    }
}
