package pl.codeleak.samples.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

import java.util.Locale;

@SpringBootApplication
public class ThymeleafApplication {

	@Bean
	public LocaleResolver localeResolver() {
		FixedLocaleResolver lr = new FixedLocaleResolver();
		lr.setDefaultLocale(Locale.of("pl"));
		return lr;
	}

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafApplication.class, args);
	}

}
