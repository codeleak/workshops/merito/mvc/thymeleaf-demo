package pl.codeleak.samples.thymeleaf;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ThymeleafFormController {
    @GetMapping("/todo")
    public String form(Model model) {
        model.addAttribute("todo", new Todo());
        return "todo";
    }

    @PostMapping("/todo")
    public String submit(@Validated @ModelAttribute Todo todo, BindingResult bindingResult, RedirectAttributes ra) {
        if (bindingResult.hasErrors()) {
            return "todo";
        }
        ra.addFlashAttribute("success", true);
        System.out.println("Todo created: [" + todo.getTitle() + "] due date: " + todo.getDueDate());
        return "redirect:todo";
    }

}
